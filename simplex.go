package simplex

import (
	"log"
	"net/http"

	"github.com/julienschmidt/httprouter"
)

type MiddlewareFunc func(w http.ResponseWriter, q *http.Request, c *C, ps Params, next http.HandlerFunc)
type HandlerFunc func(w http.ResponseWriter, q *http.Request, c *C, ps Params)

type SubFn func(mux *Mux)

//type Middleware interface {
//	ServeHTTPXM(w http.ResponseWriter, q *http.Request, c *C, ps Params, next http.HandlerFunc)
//}
//type Handler interface {
//	ServeHTTPX(w http.ResponseWriter, q *http.Request, c *C, ps Params)
//}

type Params httprouter.Params

func (ps Params) ByName(name string) string {
	return ((httprouter.Params)(ps)).ByName(name)
}

var _ http.Handler = &Mux{}

type Mux struct {
	pref string
	r    *httprouter.Router
	mids []MiddlewareFunc
}

func New() *Mux {
	this := &Mux{
		r: httprouter.New(),
	}

	return this
}

func (this *Mux) ServeHTTP(w http.ResponseWriter, q *http.Request) {
	this.r.ServeHTTP(w, q)
}

func (this Mux) copy(pref string, mid ...MiddlewareFunc) *Mux {
	this.pref += pref
	this.mids = append(this.mids, mid...)
	return &this
}

func (this *Mux) Use(mid ...MiddlewareFunc) *Mux {
	this.mids = append(this.mids, mid...)
	return this
}

func makeHandler(h HandlerFunc) httprouter.Handle {
	return func(w http.ResponseWriter, q *http.Request, ps httprouter.Params) {
		h(w, q, (*C)(q), Params(ps))
	}
}

func makeHandlerFromMiddleware(mid MiddlewareFunc, next httprouter.Handle) httprouter.Handle {
	return func(w http.ResponseWriter, q *http.Request, ps httprouter.Params) {
		mid(w, q, (*C)(q), Params(ps), func(w http.ResponseWriter, q *http.Request) {
			next(w, q, ps)
		})
	}
}

func (this *Mux) handle(methods []string, pat string, h HandlerFunc, mid ...MiddlewareFunc) *Mux {
	mids := append(this.mids, mid...)
	next := makeHandler(h)
	var last httprouter.Handle = next

	for i := len(mids) - 1; i >= 0; i-- {
		last = makeHandlerFromMiddleware(mids[i], next)
		next = last
	}

	for _, method := range methods {
		log.Println("simplex:handle:", method, this.pref+pat, len(mids), mids)
		this.r.Handle(method, this.pref+pat, last)
	}
	return this
}
func (this *Mux) Handle(method string, pat string, h HandlerFunc, mid ...MiddlewareFunc) *Mux {
	return this.handle([]string{method}, pat, h, mid...)
}

func (this *Mux) Get(pat string, h HandlerFunc, mid ...MiddlewareFunc) *Mux {
	return this.Handle("GET", pat, h, mid...)
}

func (this *Mux) Post(pat string, h HandlerFunc, mid ...MiddlewareFunc) *Mux {
	return this.Handle("POST", pat, h, mid...)
}

func (this *Mux) Any(pat string, h HandlerFunc, mid ...MiddlewareFunc) *Mux {
	this.handle([]string{"GET", "POST", "PUT", "OPTIONS"}, pat, h, mid...)
	return this
}

func (this *Mux) Sub(pat string, subFn SubFn, mid ...MiddlewareFunc) *Mux {
	subFn(this.copy(pat, mid...))
	return this
}
