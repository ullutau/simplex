package main

import (
	"fmt"
	"log"
	"net/http"

	"bitbucket.org/ullutau/simplex"
)

func Mid(msg string) simplex.MiddlewareFunc {
	return func(w http.ResponseWriter, q *http.Request, c *simplex.C, ps simplex.Params, next http.HandlerFunc) {
		log.Println("MID", msg, ps.ByName("token"))
		next(w, q)
	}
}

type Svc struct {
	Msg string
}

func (this *Svc) Hello() {
	log.Println(this.Msg)
}

func main() {

	svc := &Svc{"message0"}

	mux := simplex.New()

	mux.Use(func(w http.ResponseWriter, q *http.Request, c *simplex.C, ps simplex.Params, next http.HandlerFunc) {
		c.SetSingle(svc)
		next(w, q)
	})

	http.ListenAndServe(":8080", mux.Use(Mid("M0"), Mid("M1")).Use(Mid("M2")).
		Get("/", func(w http.ResponseWriter, q *http.Request, c *simplex.C, ps simplex.Params) {
		fmt.Fprintln(w, "HOME")
		log.Println("HOME")
	}).
		Get("/page0", func(w http.ResponseWriter, q *http.Request, c *simplex.C, ps simplex.Params) {
		fmt.Fprintln(w, "PAGE0")
		log.Println("PAGE0")
	}).
		Sub("/api/:token", func(mux *simplex.Mux) {
		mux.Get("/ep1", func(w http.ResponseWriter, q *http.Request, c *simplex.C, ps simplex.Params) {
			fmt.Fprintln(w, "EP1", ps.ByName("token"))
			log.Println("EP1", ps.ByName("token"))

			var svc *Svc
			c.GetSingle(&svc)
			svc.Hello()
		})
	}, Mid("[API AREA]"), Mid("[API AREA 2]")))
}
