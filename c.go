package simplex

import (
	"net/http"
	"reflect"

	"github.com/nbio/httpcontext"
)

type C http.Request

func (c *C) SetSingle(value interface{}) {
	t := reflect.TypeOf(value)
	for t.Kind() == reflect.Ptr {
		t = t.Elem()
	}

	httpcontext.Set(((*http.Request)(c)), reflect.New(t).Elem().Interface(), value)
}

func (c *C) GetSingle(valueOut interface{}) {
	t := reflect.TypeOf(valueOut)
	for t.Kind() == reflect.Ptr {
		t = t.Elem()
	}

	inj := httpcontext.Get(((*http.Request)(c)), reflect.New(t).Elem().Interface())

	v := reflect.ValueOf(valueOut).Elem()
	v.Set(reflect.ValueOf(inj))
}

func (c *C) Set(key interface{}, value interface{}) {
	httpcontext.Set(((*http.Request)(c)), key, value)
}

func (c *C) Get(key interface{}) interface{} {
	return httpcontext.Get(((*http.Request)(c)), key)
}

func (c *C) GetOk(key interface{}) (val interface{}, ok bool) {
	return httpcontext.GetOk(((*http.Request)(c)), key)
}

func (c *C) GetString(key interface{}) string {
	return httpcontext.GetString(((*http.Request)(c)), key)
}

func (c *C) GetAll() map[interface{}]interface{} {
	return httpcontext.GetAll(((*http.Request)(c)))
}

func (c *C) Delete(key interface{}) {
	httpcontext.Delete(((*http.Request)(c)), key)
}

func (c *C) Clear() {
	httpcontext.Clear(((*http.Request)(c)))
}
